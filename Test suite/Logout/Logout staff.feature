Feature: Logout

   Feature decription logout from Staff

   Scenario: Logout
   Given  On pin page
   When   I click "logout"
   Then   I expect system will display "เข้าสู่ระบบด้วย QR Code"

   Scenario: Logout then status of the QR code unused to POS
   Given  On POS page
   When   I click "logout"
   Then   I expect system will "unused QR Code"

   Scenario: Logout by no internet
   Given  On logout page
   When   I Close internet
   Then   I press "Logout"
   And    I expect system will alert message



