
   Feature decription transfer order from pos then staff mobile info need updated

   Scenario: Didn't order food then press transfer order
   Given  On table page
   When   I click "Table"
   Then   I select menu "ข้าวต้ม" 
   And    I did't press "send order"
   Then   I click "transfer order"
   And    I expect staff mobile will display "Disable button transfer order"

   Scenario: Send order then press transfer order
   Given  On table page
   When   I click "Table"
   Then   I select menu "ข้าวต้ม" 
   And    I click "send order"
   Then   I click "transfer order"
   And    I expect staff mobile will display "Enable button transfer order"

   Scenario: transfer order
   Given  On table page 
   When   I click "Table"
   Then   I select menu "ข้าวต้ม"  
   And    I click "send order"
   And    I click "transfer order"
   Then   I click "OK"
   And    I expect staff mobile will "transfer order success"

   Scenario: cancel transfer order
   Given  On table page
   When   I click "Table"
   Then   I select menu "ข้าวต้ม" 
   And    I click "send order"
   And    I click "transfer order"
   Then   I click "cancel"
   And    I expect staff mobile will "cancel transfer order "


