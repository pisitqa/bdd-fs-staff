Feature: Unseat

   Scenario: Unseat table after from open table
   Given  On table page
   When   I open "table"
   Then   I click "Unseat"
   And    I fill "pin 4 digit"
   And    I expect system will "canceled table"

   Scenario: Unseat table after from menu ordered 
   Given  On table page
   When   I open "table"
   Then   I click select "menu"
   And    I click "send order"
   Then   I click "Unseat"
   And    I fill "pin 4 digit"
   And    I expect system will warning message "------"
