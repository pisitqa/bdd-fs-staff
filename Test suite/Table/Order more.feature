Feature: Order

   Scenario: Select category
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   And    I expect system will display "Menu correct"

   Scenario: Menu to require option
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "สตอเบอรี่"
   And    I select option "กีวี"
   And    I click "สั่งเมนูนี้"
   And    I expect system will "Added menu and option"

   Scenario: Not choose an option
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "สตอเบอรี่"
   And    I not select option "ไม่หวาน"
   And    I click "สั่งเมนูนี้"
   And    I expect system will "Disable button"

   Scenario: Press choose ingredian
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "สตอเบอรี่"
   And    I select ingredian "ไม่หวาน"
   And    I click "สั่งเมนูนี้"
   And    I expect system will "Added menu and ingredian"

   Scenario: Not press choose ingredian
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "สตอเบอรี่"
   And    I not choose ingredian "ไม่หวาน"
   And    I click "สั่งเมนูนี้"
   And    I expect system will "not show ingredian "

   Scenario: Select menu then press back to table page
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "สตอเบอรี่"
   And    I click "สั่งเมนูนี้"
   And    I click "X"
   And    I expect system will "Remember menu item"

   Scenario: Select menu table 1 then press open table 2
   Given  On table page
   When   I click "open table 1"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "สตอเบอรี่"
   And    I click "สั่งเมนูนี้"
   Then   I click "open table 2"
   And    I click "สั่งเพิ่ม"
   And    I expect system will not remember menu item of table 1
 
   Scenario: Select menu one category
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "ข้าวต้ม"
   And    I expect system will display "Next page"

    
   Scenario: Select menu muti category
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "ข้าวต้ม"
   And    I click "สั่งเมนูนี้"
   And    I select category "Yogurt"
   Then   I select menu "สตอเบอรี่"
   And    I click "สั่งเมนูนี้"
   And    I expect system will display "Item and sum pice correct"

   Scenario: Edit order and add menu
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "ข้าวต้ม"
   And    I click "สั่งเมนูนี้"
   Then   I click "edit menu"
   And    I click "+"
   And    I click "Save"
   And    I expect system will display "Add menu in cart"

   Scenario: Edit order and Delete menu
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "ข้าวต้ม"
   And    I click "สั่งเมนูนี้"
   Then   I click "edit menu"
   And    I click "-"
   And    I click "Save"
   And    I expect system will display "Delete menu in cart"

   Scenario: Delete order
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "ข้าวต้ม"
   And    I click "สั่งเมนูนี้"
   And    I click "send item"
   Then   I click "Delete menu"
   And    I expect system will display "Delete menu in cart"

   Scenario: Delete order
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "ข้าวต้ม"
   And    I click "สั่งเมนูนี้"
   And    I click "send item"
   Then   I click "Delete menu"
   And    I expect system will display "Delete menu in cart"

   Scenario: Send order from staff mobile
   Given  On table page
   When   I click "open table"
   Then   I click "สั่งเพิ่ม"
   And    I select category "Topping"
   Then   I select menu "ข้าวต้ม"
   And    I click "สั่งเมนูนี้"
   And    I click "send item"
   Then   I click "send order"
   And    I expect system will send order to POS correct

   Scenario: Cancel order from POS
   Given  On table page
   When   I click "open bill"
   Then   I click "order"
   And    I select menu to need cancel "mango"
   Then   I click "cancel order"
   And    I fill pin "5555"
   And    I choose reason " xxxx "
   Then   I click "Done"
   And    I expect staff mobile will show "Price $XX correct"

   Scenario: Duplicate order from POS
   Given  On table page
   When   I click "open bill"
   Then   I click "order"
   And    I select menu to need cancel "mango"
   Then   I click "Duplicate order"
   And    I expect staff mobile will show "Price $XX correct"

   Scenario: Reprint order from POS
   Given  On table page
   When   I click "open bill"
   Then   I click "order"
   And    I select menu to need cancel "mango"
   Then   I click "Reprint order"
   And    I fill pin "5555"
   And    I choose reason " xxxx "
   And    I expect staff mobile will show "Price $XX correct"





