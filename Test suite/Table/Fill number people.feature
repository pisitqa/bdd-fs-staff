Feature: people

    
   Scenario: input people > maximum
   Given  On table page
   When   I click "open table"
   Then   I input "6 people"
   And    I expect system will warning "จำนวนที่นั่งสูงสุด 4 คน"

   Scenario: input people = maximum
   Given  On table page
   When   I click "open table"
   Then   I input "4 people"
   And    I expect system will save data valid

   Scenario: input people < maximum
   Given  On table page
   When   I click "open table"
   Then   I input "1 people"
   And    I expect system will save data valid

   Scenario: edit people
   Given  On table page
   When   I click "open table"
   Then   I input "1 people"
   And    I change from 1 people is 3 people
   And    I expect system will save data valid

   Scenario: Close table
   Given  On table page
   When   I click "open table"
   Then   I input "1 people"
   And    I close "table"
   And    I expect system will save data valid

   