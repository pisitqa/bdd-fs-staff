Feature: Search menu

   Scenario: Search menu by Main name
   Given  On menu page
   When   I click "open table"
   Then   I click "search button"
   And    I search by "main name"
   And    I expect system will display "menu"

   Scenario: Search menu by Secondary name 
   Given  On menu page
   When   I click "open table"
   Then   I click "search button"
   And    I search by "main name"
   And    I expect system will display "menu"

   Scenario: delete keyword to search   
   Given  On menu page
   When   I click "open table"
   Then   I click "search button"
   And    I fill keyword "ต้มยำ"
   Then   I click "X button"
   And    I expect system will display valid

   Scenario: Search menu not found 
   Given  On menu page
   When   I click "open table"
   Then   I click "search button"
   And    I find a menu that doesn't have in system "ขาไก่"
   And    I expect system will display "ไม่พบข้อมูลที่ค้นหา"