Feature: Select category

   Scenario: select category
   Given  On tabel page
   When   I click "open table"
   Then   I click choose "category"
   And    I expect system will display "Menu valid"

   Scenario: switch category
   Given  On tabel page
   When   I click "open table"
   Then   I click choose "category1"
   And    I change "category2"
   And    I expect system will display "Menu valid"

   Scenario: add category from POS
   Given  On tabel page
   When   I add "category" on pos
   Then   I click "slide bar"
   And    I click "reload button"
   And    I expect system will update "category"

   Scenario: edit category from POS
   Given  On tabel page
   When   I edit "category" on pos
   Then   I click "slide bar"
   And    I click "reload button"
   And    I expect system will update "category"
 
   Scenario: delete category from POS
   Given  On tabel page
   When   I delete "category" on pos
   Then   I click "slide bar"
   And    I click "reload button"
   And    I expect system will update "category"