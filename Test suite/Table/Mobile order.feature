Feature: Mobile order

    
   Scenario: Press mobile order for scan QR code
   Given  On table page
   When   I click "table"
   Then   I click "Mobile order"
   And    I expect system will display "Qr code"
   
   Scenario: Press back to mobile order page
   Given  On table page
   When   I click "table"
   Then   I click "Mobile order"
   And    I click "X"
   And    I expect system will back to step

   Scenario: Customer use 2 mobile scan 1 qr code 
   Given  On table page
   When   I click "table"
   Then   I click "Mobile order"
   And    I use mobile phone to 1 "scan qr code"
   Then   I click "x"
   And    I click "Mobile order"
   And    I use mobile phone to 2 "scan qr code"
   And    I expect system will use same qr code 

   Scenario: Staff close table then open new table
   Given  On table page
   When   I click open "table"
   Then   I click "Mobile order"
   And    I use mobile phone "scan qr code"
   And    I expect system will use new qr code for new table



