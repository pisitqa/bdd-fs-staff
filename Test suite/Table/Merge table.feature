Feature: Merge table

   Scenario: Merge table before order food from POS
   Given  On table page
   When   I click "open table"
   Then   I click "merge table"
   And    I click "OK"
   Then   I choose menu "ข้าวต้ม"
   And    I click "send order"
   And    I expect staff mobile will display "Sum menu and Sum price" valid

   Scenario: Order food before Merge table from POS 
   Given  On table page
   When   I click "open table"
   Then   I choose menu "ข้าวต้ม"
   And    I click "send order"
   Then   I click "merge table"
   And    I click "OK"
   And    I expect staff mobile will display "Sum menu and Sum price" valid

   Scenario: Close table after merge table
   Given  On table page
   When   I click "open table"
   Then   I choose menu "ข้าวต้ม"
   And    I click "send order"
   Then   I click "merge table"
   And    I click "OK"
   Then   I click "paid"
   And    I expect staff mobile will display "Close table" 

