
   Feature decription change table from pos then staff mobile info need updated

   Scenario: Didn't send order then press change table
   Given  On table page
   When   I click "Table"
   Then   I select menu "ข้าวต้ม" 
   And    I did't press "send order"
   Then   I click "change table"
   And    I click "OK"
   And    I expect staff mobile will "Change table success"

   Scenario: Send order then press change table
   Given  On table page
   When   I click "Table"
   Then   I select menu "ข้าวต้ม" 
   And    I click "send order"
   Then   I click "change table"
   And    I click "OK"
   And    I expect staff mobile will "Change table success"

   Scenario: Cancel change table
   Given  On table page
   When   I click "Table"
   Then   I select menu "ข้าวต้ม" 
   And    I click "send order"
   And    I click "change table"
   Then   I click "cancel"
   And    I expect staff mobile will "cancel change table"
