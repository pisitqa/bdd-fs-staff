Feature: Restaurant name and branch name

   Scenario: Restaurant info
   Given  On slide bar page
   When   I click "slide bar button"
   Then   I expect system will display "Restaurant name"
   And    I expect system will display "Branch name"

   Scenario: Edit restaurant name from POS
   Given  On slide bar page
   When   I edit "restaurant name" from POS
   Then   I click "slide bar button"
   And    I click "reload button"
   And    I expect system will display "Restaurant name" correct

   Scenario: Edit Branch name from POS
   Given  On slide bar page
   When   I edit "Branch name" from POS
   Then   I click "slide bar button"
   And    I click "reload button"
   And    I expect system will display "Branch name" correct

