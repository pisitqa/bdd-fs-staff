Feature: setting

   Scenario: Main name
   Given  On slide bar page
   When   I click "ตั้งค่า"
   Then   I expect system will display "หน้าตั้งค่า"
   And    I Click "ชื่อหลัก"
   And    I expect system will display "เมนูชื่อหลัก"

   Scenario: Secondary name
   Given  On slide bar page
   When   I click "ตั้งค่า"
   Then   I expect system will display "หน้าตั้งค่า"
   And    I Click "ชื่อรอง"
   And    I expect system will display "เมนูชื่อรอง"

   Scenario: Enable "ปิดข้อมูลการสั่งอาหาร"
   Given  On slide bar page
   When   I click "ตั้งค่า"
   Then   I click "Enable ปิดข้อมูลการสั่งอาหาร"
   And    I expect system will display "สั่งอาหารได้"

   Scenario: Unable "ปิดข้อมูลการสั่งอาหาร"
   Given  On slide bar page
   When   I click "ตั้งค่า"
   Then   I click "Unable ปิดข้อมูลการสั่งอาหาร"
   And    I expect system will display "สั่งอาหารไม่ได้"

   Scenario: Enable "เรียกเช็กบิล"
   Given  On slide bar page
   When   I click "ตั้งค่า"
   Then   I click "Enable ปิดข้อมูลการสั่งอาหาร"
   And    I expect system will display "สั่งอาหารได้"

   Scenario: Unable "เรียกเช็กบิล"
   Given  On slide bar page
   When    I click "ตั้งค่า"
   Then    I click "Unable ปิดข้อมูลการสั่งอาหาร"
   And     I expect system will display "สั่งอาหารไม่ได้"