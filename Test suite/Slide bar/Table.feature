 Scenario: Add table from POS
   Given  On table page
   When   I add "table" from POS
   Then   I click "slide bar"
   And    I click "Reload button"
   And    I expect system will "Refresh data from POS valid"

   Scenario: Edit table from POS
   Given  On table page
   When   I edit "table" from POS
   Then   I click "slide bar"
   And    I click "Reload button"
   And    I expect system will "Refresh data from POS valid"

   Scenario: Delete table from POS
   Given  On table page
   When   I delete "table" from POS
   Then   I click "slide bar"
   And    I click "Reload button"
   And    I expect system will "Refresh data from POS valid"