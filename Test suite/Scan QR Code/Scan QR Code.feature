Feature: Scan QR Code

   Scenario: Login with QR
   Given  On home page
   When   I click "Login with QR Code"
   Then   I expect system will "open Camera"

   Scenario: Reset QR from POS
   Given  On home page
   When   I click "Reset QR"
   Then   I expect staff will "disconnect mobile staff"

Scenario: After reset QR from POS then scan again
   Given  On home page
   When   I click "Login with QR Code"
   Then   I click "Reset QR"
   Then   I click "Login with QR Code"
   And    I expect staff will "connect mobile staff success"




