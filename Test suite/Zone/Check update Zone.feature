Feature: Zone

   Scenario: Zone
   Given  On table page
   When   I click "Zone1"
   Then   I expect system will display "table in zone1"

   Scenario: Select zone
   Given  On table page
   When   I click "Zone2"
   Then   I expect system will display "table in zone2"

   Scenario: Add zone from POS
   Given  On table page
   When   I add "zone" from POS
   Then   I click "slide bar"
   And    I click "Reload button"
   And    I expect system will "Refresh data from POS valid"

   Scenario: Edit zone from POS
   Given  On table page
   When   I edit "zone" from POS
   Then   I click "slide bar"
   And    I click "Reload button"
   And    I expect system will "Refresh data from POS valid"

   Scenario: Delete zone from POS
   Given  On table page
   When   I delete "zone" from POS
   Then   I click "slide bar"
   And    I click "Reload button"
   And    I expect system will "Refresh data from POS valid"