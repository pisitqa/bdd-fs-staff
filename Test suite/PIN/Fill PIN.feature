Feature: PIN

   Feature decription fill pin

   Scenario: Fill pin valid
   Given  On pin page
   When   I fill "pin 4 digit"
   Then   I expect system will display "table page"

   Scenario: Fill pin invalid
   Given  On pin page
   When   I fill "pin 4 digit"
   Then   I expect system will warning message "รหัสผ่านไม่ถูกต้อง"

   Scenario: Fill pin over digit
   Given  On pin page
   When   I fill "pin 5 digit"
   Then   I expect system will warning message "รหัสผ่านไม่ถูกต้อง"

   Scenario: Delete pin
   Given  On pin page
   When   I fill "pin 3 digit"
   Then   I expect system will can deleted

   Scenario: Change pin from POS
   Given  On pin page
   When   I fill "pin 4 digit"
   Then   I expect system will can working correct

