  Scenario: Verify QR
   Given  On table page
   When   I click "table"
   Then   I click "Bill"
   And    I click "ชำระด้วย QR"
   And    I expect system will display "QR code"

   Scenario: Verify paid by QR
   Given  On table page
   When   I click "table"
   Then   I click "Bill"
   And    I click "ชำระด้วย QR"
   And    I expect system will "Paid Success"

   Scenario: Verify QR already paid
   Given  On table page
   When   I click "table"
   Then   I click "Bill"
   And    I click "ชำระด้วย QR"
   And    I expect system will display "QR Unused"